﻿using System.Collections.Generic;

namespace WpfSnake
{
    public class Snake
    {
        public int Length { get; set; }
        public List<Point> SegmentsPositions { get; set; }

        public Snake()
        {
            Length = 1;
            SegmentsPositions = new List<Point>();
            SegmentsPositions.Add(new Point(180, 180));
        }
    }
}
