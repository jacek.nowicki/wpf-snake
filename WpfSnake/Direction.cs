﻿namespace WpfSnake
{
    public enum Direction
    {
        Top,
        Bottom,
        Left,
        Right
    }
}
