﻿namespace WpfSnake
{
    public class Point
    {
        public int X { get; set; }
        public int Y { get; set; }

        public Point()
        {
            X = default;
            Y = default;
        }

        public Point(int x, int y)
        {
            X = x;
            Y = y;
        }

        public static bool operator ==(Point a, Point b)
        {
            if (a.X == b.X &&
                a.Y == b.Y)
                return true;
            return false;
        }

        public static bool operator !=(Point a, Point b)
        {
            if (a == b)
                return false;
            return true;
        }

        public override bool Equals(object obj)
        {
            if (obj.GetType() == typeof(Point))
                return this == (Point)obj;
            return false;
        }
    }
}
