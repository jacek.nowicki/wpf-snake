﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfSnake
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private Snake snake;
        private Timer snakeTimer;
        private bool isSnakeStarted;
        private bool isGameOver;
        private double timerInterval;
        private Direction snakeDirection;
        private Random random;
        private Point apple = new Point();

        public MainWindow()
        {
            InitializeComponent();

            random = new Random();

            // Tymaczasowy wąż...
            snake = new Snake();
            //snake.SegmentsPositions.Add(new Point(180, 160));
            //snake.SegmentsPositions.Add(new Point(180, 140));
            //snake.SegmentsPositions.Add(new Point(180, 120));
            //snake.SegmentsPositions.Add(new Point(180, 100));
            //snake.SegmentsPositions.Add(new Point(180, 80));

            GenerateNewApple();
            //apple = new Point(
            //    random.Next(0, 19) * 20,
            //    random.Next(0, 19) * 20);

            isSnakeStarted = false;
            timerInterval = 150;
            snakeTimer = new Timer(timerInterval);
            snakeTimer.Elapsed += SnakeTimer_Elapsed;
        }

        private void SnakeTimer_Elapsed(object sender, ElapsedEventArgs e)
        {
            Application.Current.Dispatcher.Invoke(() => MoveSnake(snakeDirection));
        }

        private void Window_KeyDown(object sender, KeyEventArgs e)
        {
            switch (e.Key)
            {
                case Key.Escape:
                    break;

                case Key.Space:
                    if (snakeTimer.Enabled)
                    {
                        PauseGrid.Visibility = Visibility.Visible;
                        PauseText.Text = "GAME PAUSED";
                        snakeTimer.Stop();
                    }
                    else
                    {
                        PauseGrid.Visibility = Visibility.Collapsed;
                        snakeTimer.Start();
                    }
                    break;

                case Key.Enter:
                    if (isGameOver)
                    {
                        snake = new Snake();
                        //Canvas.SetTop(SnakeHead, 180);
                        //Canvas.SetLeft(SnakeHead, 180);
                        PauseGrid.Visibility = Visibility.Collapsed;
                        isGameOver = false;
                        isSnakeStarted = false;
                    }
                    break;

                case Key.Left:
                case Key.A:
                    if (isSnakeStarted)
                        ChangeDirection(Direction.Left);
                    else
                        StartSnake(Direction.Left);
                    break;

                case Key.Up:
                case Key.W:
                    if (isSnakeStarted)
                        ChangeDirection(Direction.Top);
                    else
                        StartSnake(Direction.Top);
                    break;

                case Key.Right:
                case Key.D:
                    if (isSnakeStarted)
                        ChangeDirection(Direction.Right);
                    else
                        StartSnake(Direction.Right);
                    break;

                case Key.Down:
                case Key.S:
                    if (isSnakeStarted)
                        ChangeDirection(Direction.Bottom);
                    else
                        StartSnake(Direction.Bottom);
                    break;
            }
        }

        private void StartSnake(Direction direction)
        {
            isSnakeStarted = true;
            snakeTimer.Start();
            snakeDirection = direction;
        }

        private void ChangeDirection(Direction newDirection)
        {
            switch (snakeDirection)
            {
                case Direction.Top:
                case Direction.Bottom:
                    if (newDirection == Direction.Left ||
                        newDirection == Direction.Right)
                        snakeDirection = newDirection;
                    break;

                case Direction.Left:
                case Direction.Right:
                    if (newDirection == Direction.Top ||
                        newDirection == Direction.Bottom)
                        snakeDirection = newDirection;
                    break;
            }
        }

        private void MoveSnake(Direction direction)
        {
            snakeDirection = direction;
            var position = snake.SegmentsPositions.First();
            var oldHeadPosition = new Point(position.X, position.Y);

            switch (direction)
            {
                case Direction.Top:
                    if (position.Y <= 0)
                        StopGame();
                    else
                        position.Y -= 20;
                    break;

                case Direction.Bottom:
                    if (position.Y >= 380)
                        StopGame();
                    else
                        position.Y += 20;
                    break;

                case Direction.Left:
                    if (position.X <= 0)
                        StopGame();
                    else
                        position.X -= 20;
                    break;

                case Direction.Right:
                    if (position.X >= 380)
                        StopGame();
                    else
                        position.X += 20;
                    break;
            }

            CheckSnakePosition();
            ChangePointPosition(1, oldHeadPosition);

            DrawGraphics();
        }

        private void DrawGraphics()
        {
            Board.Children.Clear();
            foreach (var segment in snake.SegmentsPositions)
            {
                Board.Children.Add(new Rectangle
                {
                    Width = 20,
                    Height = 20,
                    Fill = Brushes.OrangeRed,
                    Stroke = Brushes.Gray,
                    StrokeThickness = 1,
                    RenderTransform = new TranslateTransform
                    {
                        X = segment.X,
                        Y = segment.Y
                    }
                });
            }

            Board.Children.Add(new Rectangle
            {
                Width = 20,
                Height = 20,
                Fill = Brushes.CadetBlue,
                Stroke = Brushes.Gray,
                StrokeThickness = 1,
                RenderTransform = new TranslateTransform
                {
                    X = apple.X,
                    Y = apple.Y
                }
            });
        }

        private void StopGame()
        {
            snakeTimer.Stop();
            isGameOver = true;
            PauseGrid.Visibility = Visibility.Visible;
            PauseText.Text = "GAME OVER!";
        }
    
        private void ChangePointPosition(int index, Point newPosition)
        {
            if (index >= snake.SegmentsPositions.Count)
                return;

            var oldPosition = new Point
            {
                X = snake.SegmentsPositions[index].X,
                Y = snake.SegmentsPositions[index].Y
            };

            snake.SegmentsPositions[index] = newPosition;
            ChangePointPosition(index + 1, oldPosition);
        }

        private void CheckSnakePosition()
        {
            var snakeHeadPosition = snake.SegmentsPositions.First();

            var snakeTail = snake.SegmentsPositions
                .TakeLast(snake.SegmentsPositions.Count - 1);
            if (snakeTail.Any(x => x == snakeHeadPosition))
                StopGame();

            if (snakeHeadPosition == apple)
            {
                // Tutaj dodanie nowego segmentu węża
                snake.SegmentsPositions.Add(new Point());
                Score.Text = $"Wynik: {snake.SegmentsPositions.Count - 1}";

                GenerateNewApple();
            }
        }

        private void GenerateNewApple()
        {
            Point point;
            do
            {
                point = new Point(
                    random.Next(0, 19) * 20,
                    random.Next(0, 19) * 20);

            } while (point == apple || 
                    snake.SegmentsPositions.Any(x => x == point));

            apple = point;
        }
    }
}
